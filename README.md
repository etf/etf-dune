# ETF-DUNE

DUNE ETF plugins and payload

# Tests performed

### 1. test_basic_functionality
* Test if CVMFS is installed (rpm is available)
  * NO : Check for NFS mounting of CVMFS repositories
  * Yes : Check for CVMFS config, Check that VO_SW_DIR env variable exists

### 2. test_stashcache
* Check if the following file exists and can be read
  * /cvmfs/dune.osgstorage.org/pnfs/fnal.gov/usr/dune/persistent/stash/test.stashdune.1M

### 3. test_repository : dune.opensciencegrid.org
* Check the repository in detail
  * cvmfs_readconfig
  * ls
  * CVMFS version
  * Repository version
  * Check CVMFS I/O errors (get_xattr CVMFS_IOERR nioerr $mnt_repo $strict)
  * Number of used file descriptors (Warning if >80, Error if >95)
  * Number of catalogs (relative to # file descriptors above)
  * Check memory consumed by CVMFS w.r.t. total memory available
  * Look for .cvmfsdirtab
  * set VO_SW_DIR_OK if VO_SW_DIR points to the correct repository
  * Some "network connectivity" tests

### 4. Check VO_SW_DIR_OK (from previous test_repository)

### 5. test_repository dune.osgstorage.org
* Check the repository in detail
  * cvmfs_readconfig
  * ls
  * CVMFS version
  * Repository version
  * Check CVMFS I/O errors (get_xattr CVMFS_IOERR nioerr $mnt_repo $strict)
  * Number of used file descriptors (Warning if >80, Error if >95)
  * Number of catalogs (relative to # file descriptors above)
  * Check memory consumed by CVMFS w.r.t. total memory available
  * Look for .cvmfsdirtab
  * set VO_SW_DIR_OK if VO_SW_DIR points to the correct repository
  * Some "network connectivity" tests
* note this is an optional repository
  * the test will stop on the first ERROR (but not updating the return code)

### 6. test_simulation
* Run a simple 5 event cosmic-ray test_simulation (following 3 lines of code)
```
  source /cvmfs/dune.opensciencegrid.org/products/dune/setup_dune.sh > /dev/null 2>&1;
  setup dunetpc v09_13_00 -q e19:prof > /dev/null 2>&1;
  lar -n 5 -c $PWD/etf/probes/org.dune/prod4_gen_protoDUNE_cosmics_1GeV_local.fcl -o gen.root > /dev/null 2>&1;
```
* FCL file used is : "etf/probes/prod4_gen_protoDUNE_cosmics_1GeV_local.fcl"
  * Simple link : https://gitlab.cern.ch/etf/etf-dune/-/blob/master/src/probes/prod4_gen_protoDUNE_cosmics_1GeV_local.fcl
