FROM gitlab-registry.cern.ch/etf/docker/etf-base:el9

ENV NSTREAM_ENABLED=0

# Streaming
COPY ./config/ocsp_handler.cfg /etc/nstream/

# # # RUN yum -y install python3-gfal2 python3-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp gfal2-plugin-xrootd xrootd-client

# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
COPY ./config/client.conf /opt/omd/sites/$CHECK_MK_SITE/.arc/
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY ./config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

################################
# OSG Middleware
# RUN dnf config-manager --set-enabled epel \
#     && dnf -y install nagios-plugins \

# ETF WN-qFM payload
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
RUN cp -r /usr/lib/python3.9/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages

COPY ./src/probes /usr/libexec/grid-monitoring/probes/org.dune/wnjob/org.dune/probes/org.dune
####################################

# ETF local checks
COPY config/dune_plugin.py /usr/lib/ncgx/x_plugins/
COPY ./config/wlcg_dune.cfg /etc/ncgx/metrics.d/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.dune/wnjob

# ETF config
COPY ./config/dune_checks.cfg /etc/ncgx/conf.d/
COPY ./config/ncgx.cfg /etc/ncgx/

COPY ./config/add-keys.sh /opt/omd/sites/etf/.oidc-agent/

EXPOSE 80 443 6557
ENTRYPOINT ["/usr/sbin/init"]
