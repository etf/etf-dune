#!/bin/bash
set -e

# Source the initialization script
source /usr/bin/etf-init.sh

function start_systemd() {
    echo "Starting systemd..."
    # Ensure cgroups directories exist and are properly mounted
    mkdir -p /sys/fs/cgroup/systemd
    mountpoint -q /sys/fs/cgroup/systemd || mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd || {
        echo "Failed to mount cgroup: permission denied. Trying to continue..."
    }

    # Start systemd
    exec /usr/sbin/init &
    # Wait for systemd to fully start
    sleep 5
}

cat << "EOF"
 _____ _____ _____   ____  _   _ _   _ _____
| ____|_   _|  ___| |  _ \| | | | \ | | ____|
|  _|   | | | |_    | | | | | | |  \| |  _|
| |___  | | |  _|   | |_| | |_| | |\  | |___
|_____| |_| |_|     |____/ \___/|_| \_|_____|
=============================================
EOF

print_header

etf_update

start_systemd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

start_oidc_agent

echo "Fetching DUNE credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /dune/Role=Production --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo dune --lifetime 24 --name NagiosRetrieve-ETF-dune -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem --dune-Role_production --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /dune/Role=ETF --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo dune --lifetime 24 --name NagiosRetrieve-ETF-dune -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem --dune-Role_etf --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Initialising tokens ..."
#su - etf -c "oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_dune_ce.key etf_dune_ce"
#su - etf -c "/usr/lib64/nagios/plugins/refresh_token -t 890 --token-config dune-ce --token-time 21600 -x /opt/omd/sites/etf/etc/nagios/globus/dune.token"

etf_wait
