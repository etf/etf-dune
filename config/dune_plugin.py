import logging
import json

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

FLAVOR_MAP = {'ARC-CE': 'arc',
              'HTCONDOR-CE': 'condor',
              'GLOBUS': 'gt',
              'OSG-CE': 'gt'}

CE_STATE_METRICS = [
    'org.sam.CONDOR-JobState-/dune/Role=Production']

CE_METRICS = (
    'org.sam.CONDOR-JobSubmit-/dune/Role=Production',
)

WN_METRICS = {
    'WN-cvmfs': 'org.dune.WN-cvmfs-/dune/Role=Production',
}


def run(url, ipv6=False):

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add host groups
    sites = feed.get_groups("DUNE_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.items():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    c.add_all(CE_METRICS, tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(WN_METRICS.values(), tags=["ARC-CE", "HTCONDOR-CE"])

    # ETF env - environment variables to export on the worker node (global for all sites), such as:
    # ETF_TESTS - points to a list of WN tests to execute (stored in WN_METRICS)
    with open('/tmp/etf-env.sh', 'w') as etf_env:
        etf_env.write('ETF_TESTS={}\n'.format(','.join(['etf/probes/org.dune/'+m for m in WN_METRICS.keys()])))

    # ETF WN-qFM config - maps WN tests to metrics (WN-cvmfs -> org.lhcb.WN-cvmfs-/lhcb/Role=Production)
    with open('/tmp/etf_wnfm.json', 'w') as etf_wnfm:
        json.dump({'wn_metric_map': WN_METRICS, 'counter_enabled': True}, etf_wnfm)

    # Add queues
    for service in services:
        host = service[0]
        flavor = service[1]
        if flavor not in ["ARC-CE", "HTCONDOR-CE"]:
            continue
        if flavor == 'HTCONDOR-CE' and 'fnal.gov' in host:
            # special handling for FNAL (no queues, special role)
            c.add('org.sam.CONDOR-JobState-/dune/Role=Production', hosts=(service[0],),
                  params={'args': {"-x": "/opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--dune-Role_etf",
                                   '--resource': 'htcondor://%s' % service[0]}})
            continue
        if flavor == 'HTCONDOR-CE':
            # special handling for HTCONDOR-CE, no queues
            for m in CE_STATE_METRICS:
                c.add(m, hosts=(service[0],), params={'args': {'--resource': 'htcondor://%s' % service[0]}})
            continue
        ce_resources = feed.get_ce_resources(host, flavor)
        if ce_resources:
            batch = ce_resources[0][0]
            queue = ce_resources[0][1]
            if not batch:
                batch = "nopbs"
            if flavor not in FLAVOR_MAP.keys():
                log.warning("Unable to determine type for flavour %s" % flavor)
                continue
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, 'nosched', batch, queue)
            for m in CE_STATE_METRICS:
                c.add(m, hosts=(service[0],), params={'args': {'--resource': '%s' % res}})
        else:
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, 'nosched', 'nobatch', 'noqueue')
            for m in CE_STATE_METRICS:
                c.add(m, hosts=(service[0],), params={'args': {'--resource': '%s' % res}})

    # Custom hosts
    c_hosts = [('arc-ce-test02.gridpp.rl.ac.uk', 'ARC-CE', '')]    # add [(host, flavor, queue), (host2, flavor2, queue2), ...]
    for h, f, q in c_hosts:
        if not q:
            q = 'noqueue'
        res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[f], h, 'nosched', 'nobatch', q)
        c.add('org.sam.CONDOR-JobState-/dune/Role=Production', hosts=(h,), params={'args': {'--resource': '%s' % res}})
        c.add('org.sam.CONDOR-JobSubmit-/dune/Role=Production', hosts=(h,), params={'_unique_tag': f})
        c.add('org.dune.WN-cvmfs-/dune/Role=Production', hosts=(h,), params={'_unique_tag': f})

    c.serialize()
