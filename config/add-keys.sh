#!/bin/bash
exit
source /opt/omd/sites/etf/.oidc-agent/oidc-env.sh
cp -f /etc/grid-security/tokens/* /opt/omd/sites/etf/.oidc-agent/
cp /opt/omd/sites/etf/.oidc-agent/etf_dune_ce{.K8s,}
/usr/bin/oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_dune_ce.key etf_dune_ce
/usr/lib64/nagios/plugins/refresh_token -t 890 --token-config dune-ce --token-time 21600 -x /opt/omd/sites/etf/etc/nagios/globus/dune.token
